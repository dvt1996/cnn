
## LENET5
![](https://cdn-images-1.medium.com/max/800/0*V1vb9SDnsU1eZQUy.jpg)
Được giới thiệu vào năm 1994 và đây một trong những mạng nơ-ron tích chập đầu tiên được giới thiệu bởi Yann LeCun đã thúc đẩy lĩnh vực *Deep Learning* phát triển.  Công trình tiên phong này của Yann LeCun được đặt tên là LeNet5.

Kiến trúc LeNet5 đặt nền tảng cơ bản cho việc nhận ra rằng các đặc trưng của hình ảnh được phân phối trên toàn bộ hình ảnh, và việc tính toán tích chập với các thông số có thể học được là một cách hiệu quả để trích xuất các đặc trưng tại nhiều vị trí với một vài thông số. Vào thời điểm đó, GPU chưa phát triển để hỗ trợ cho việc huấn luyện mạng, thấm chí tốc độ của CPU vẫn còn chậm. Do đó việc có thể lưu các thông số và tính toán là việc ưu tiên hơn cả. Điều này trái ngược với việc sử dụng từng pixel làm đầu vào riêng biệt của mạng nơron nhiều lớp lớn.  LeNet5 giải thích rằng những thứ này không nên được sử dụng trong lớp đầu tiên vì hình ảnh có mối tương quan không gian cao và việc sử dụng từng pixel riêng lẻ của hình ảnh như các **đặc điểm đầu vào** riêng biệt sẽ không tận dụng được những tương quan này.

Các đặc điểm của LeNet5 được tóm tắt:
> 
- Mạng nơ-ron tích chập sử dụng tuần tự 3 lớp: tích chập, pooling, non-linearity
- Sử dụng tích chập để trích xuất đặc trưng của ảnh.
- Cuối cùng sử dụng mạng nơ-ron nhiều lớp để làm bộ phân loại.
Kiến trúc này là nguồn gốc của các mô hình mạng sau này, và là nguồn cảm hứng cho nhiều người trong lĩnh lực này.

## AlexNet:
Vào năm 2012, Alex Krizhevsky đã giới thiệu AlexNet - một phiên bản sâu hơn và rộng hơn nhiều của LeNet và giành chiến thắng trong cuộc thi ImageNet nổi tiếng.
![](https://cdn-images-1.medium.com/max/1536/1*qyc21qM0oxWEuRaj-XJKcw.png)
AlexNet mở rộng những ý tưởng từ LeNet thành một mạng nơron lớn hơn nhiều có thể được sử dụng để *học* các đối tượng phức tạp hơn và phân cấp đối tượng. Đóng góp của AlexNet trong lĩnh vực này là:
- Sử dụng hàm ReLU để làm activation-function, giảm thời gian huấn luyện mạng.
- Sử dụng kỹ thuật *dropout* để bỏ qua có chọn lọc các nơ-ron đơn lẻ trong quá trình huấn luyện, mục đích để tránh hiện tượng overfit.
- Sử dụng GPU NVIDIA GTX 580 để giảm thời gian đào tạo

Vào thời điểm đó GPU có số lượng lõi lớn hơn nhiều so với CPU và cho phép thời gian đào tạo nhanh hơn gấp 10 lần do đó cho phép sử dụng các tập dữ liệu lớn hơn và cũng có hình ảnh lớn hơn.
Sự thành công của AlexNet đã bắt đầu một cuộc cách mạng nhỏ. Mạng nơron tích chập bây giờ nay đã trở thành tên gọi gắn liền với Deep Learning.

## VGG:
Thay vì sử dụng bộ lọc có kích thước thước đối lớn lớn như của AlexNet (11x11) và ZFNet (7x7), VGG lại sử dụng bộ lọc có kích thước 3x3 nhỏ hơn nhiều trong suốt mô hình.
Điều này dường như trái ngược với các nguyên tắc của LeNet - sử dụng các bộ lọc có kích thước lớn để trích đặc trưng của ảnh. Nhưng ưu điểm lớn của VGG là nhận ra rằng sử dụng chồng của nhiều lớp tích chập 3×3 theo trình tự có thể mô phỏng ảnh hưởng của các trường tiếp nhận lớn hơn (sử dụng 2 chồng 2 lớp tích chập 3x3 có hiệu quả tương tự sử như 5×5, hoặc 3 lớp 3x3 lại có hiệu quả tương tự như 7×7). Những ý tưởng này cũng được sử dụng trong các kiến ​​trúc mạng gần đây như Inception và ResNet.
![](https://cdn-images-1.medium.com/max/800/0*HREIJ1hjF7z4y9Dd.jpg)

Đầu ra của khối các lớp tích chập sẽ được cho qua 3 lớp Fully-connected (FC): 2 lớp đầu có
4096 nơ-ron, lớp thứ 3 thực hiện phân loại 1000 lớp của tập dữ liệu ILSVRC. Mỗi lớp ẩn đểu sử dụng hàm kích hoạt phi tuyến ReLU. Việc sử dụng kết hợp chồng nhiều lớp tích chập và theo sau là ReLU làm giảm số lượng tham số và tăng độ phi tuyến tính của mạng.


## ResNet
ResNet có một ý tưởng đơn giản nạp đầu ra của hai lớp liên tiếp và cũng bỏ qua đầu vào cho các lớp tiếp theo.
![](https://cdn-images-1.medium.com/max/800/0*0r0vS8myiqyOb79L.jpg)
Mô hình này bỏ qua hai lớp và được áp dụng cho quy mô lớn. Bỏ qua sau 2 lớp là một trực giác quan trọng vì bỏ qua một lớp đơn lẻ không có nhiều cải tiến.  Bởi 2 lớp có thể được coi là một phân loại nhỏ hoặc một mạng trong mạng. Đây cũng là lần đầu tiên một mạng lưới hàng trăm thậm chí 1000 lớp được đào tạo.
ResNet với một số lượng lớn các lớp bắt đầu sử dụng lớp bottleneck
![](https://cdn-images-1.medium.com/max/800/0*9tCUFp28oQGOK6bE.jpg)
Lớp này làm giảm số lượng các đặc trưng ở mỗi lớp bằng cách sử dụng lớp tích chập kích thước 1x1 với một đầu ra nhỏ hơn (thường bằng 1/4 đầu vào) và sau đó là một lớp tích chập kích thước 3×3 và một lần nữa lớp tích chập kích thước 1x1 cho một số lượng đặc trưng lớn hơn. Điều này làm giảm sự tính toán cho mạng.





Ref:

- https://trantheanh.github.io/2018/05/10/ML-22/
- **http://dataconomy.com/2017/04/history-neural-networks/**
- https://medium.com/@14prakash/understanding-and-implementing-architectures-of-resnet-and-resnext-for-state-of-the-art-image-cf51669e1624